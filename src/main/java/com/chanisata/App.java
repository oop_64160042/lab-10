package com.chanisata;

public class App {
    public static void main(String[] args) {
        Rectangle rec1 = new Rectangle(5, 3);
        System.out.println(rec1.toString());
        System.out.println("Area :" + rec1.calArea());
        System.out.println("Perimeter :" + rec1.calPerimeter());
        System.out.println();

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println("Area :" + rec2.calArea());
        System.out.println("Perimeter :" + rec2.calPerimeter());
        System.out.println();

        Circle cir1 = new Circle(2);
        System.out.println(cir1);
        System.out.printf("%s Area : %.3f \n", cir1.toString(), cir1.calArea());
        System.out.printf("%s Perimeter : %.3f \n", cir1.toString(), cir1.calPerimeter());
        System.out.println();

        Circle cir2 = new Circle(3);
        System.out.println(cir2);
        System.out.printf("%s Area : %.3f \n", cir2.toString(), cir2.calArea());
        System.out.printf("%s Perimeter : %.3f \n", cir2.toString(), cir2.calPerimeter());
        System.out.println();

        Triangle Tri = new Triangle(2, 2, 2);
        System.out.println(Tri);
        System.out.printf("Area : %.3f \n", Tri.calArea());
        System.out.printf("Perimeter : %.3f \n", Tri.calPerimeter());
        System.out.println();
    }
}
