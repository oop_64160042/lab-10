package com.chanisata;

public class Circle extends Shape {
    private double r;

    public Circle(double r) {
        super("Circle");
        this.r = r;
    }

    public double getR() {
        return r;
    }

    @Override
    public double calArea() {
        return Math.PI * (this.r * this.r);
    }

    @Override
    public double calPerimeter() {
        return 2 * Math.PI * this.r;
    }

    @Override
    public String toString() {
        return this.getName() + " Radius :" + this.r;
    }
}
